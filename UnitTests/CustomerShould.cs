﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class CustomerShould
    {
        [Theory]
        [MemberData(nameof(BeInLegalAgeData))]
        public void BeInLegalAge(DateTime birthDate)
        {
            Customer sut = new Customer
            {
                DateOfBirth = birthDate
            };

            Assert.True(sut.InLegalAge);
        }

        public static IEnumerable<object[]> BeInLegalAgeData =>
            new List<object[]>
            {
                new object[] { new DateTime(1989, 9, 29) },
                new object[] { new DateTime(2020, 1, 1) },
                new object[] { new DateTime(1995, 3, 20) },
            };
        }
}
