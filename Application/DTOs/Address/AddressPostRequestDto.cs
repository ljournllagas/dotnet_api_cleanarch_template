﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.DTOs.Address
{
    public class AddressPostRequestDto
    {
        public string FullAddress { get; set; }

        public int ZipCode { get; set; }

        public Guid CustomerId { get; set; }
    }


}
