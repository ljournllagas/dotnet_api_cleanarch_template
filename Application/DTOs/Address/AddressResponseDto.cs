﻿namespace Application.DTOs.Address
{
    public class AddressResponseDto
    {
        public int Id { get; set; }

        public string FullAddress { get; set; }

        public int ZipCode { get; set; }
    }

}
