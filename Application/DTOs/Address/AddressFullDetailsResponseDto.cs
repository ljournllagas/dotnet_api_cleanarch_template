﻿using System;

namespace Application.DTOs.Address
{
    public class AddressFullDetailsResponseDto
    {
        public int Id { get; set; }

        public string FullAddress { get; set; }

        public int ZipCode { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public string EmailAddress { get; set; }

        public DateTimeOffset DateOfBirth { get; set; }
    }

}
