﻿using System;

namespace Application.DTOs.Address
{
    public class AddressPutRequestDto
    {
        public int Id { get; set; }

        public string FullAddress { get; set; }

        public int ZipCode { get; set; }

        public Guid CustomerId { get; set; }
    }


}
