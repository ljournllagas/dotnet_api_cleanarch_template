﻿using System;

namespace Application.DTOs.Product
{
    public class ProductResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public DateTimeOffset ReleaseDate { get; set; }
    }
}
