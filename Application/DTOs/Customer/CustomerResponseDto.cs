﻿using Application.DTOs.Address;
using System;
using System.Collections.Generic;

namespace Application.DTOs.Customer
{
    public class CustomerResponseDto
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public string EmailAddress { get; set; }

        public DateTimeOffset DateOfBirth { get; set; }

        public bool InLegalAge { get; set; }

    }
}
