﻿using Application.Common.Wrappers;
using Application.DTOs.Address;
using Application.Interfaces.Repositories;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Address.Queries
{
    public class GetAddressWithFullDetailsQuery : IRequest<Response<IEnumerable<AddressFullDetailsResponseDto>>>
    {
        public class GetAddressWithFullDetailsQueryHandler : IRequestHandler<GetAddressWithFullDetailsQuery,
            Response<IEnumerable<AddressFullDetailsResponseDto>>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public GetAddressWithFullDetailsQueryHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<IEnumerable<AddressFullDetailsResponseDto>>> Handle(GetAddressWithFullDetailsQuery request, CancellationToken cancellationToken)
            {
                var entity = await _unitOfWork.Address.GetAllAsync<AddressFullDetailsResponseDto>();

                return new Response<IEnumerable<AddressFullDetailsResponseDto>>(entity);
            }
        }
    }
}