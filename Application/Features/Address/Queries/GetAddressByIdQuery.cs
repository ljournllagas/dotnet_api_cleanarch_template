﻿using Application.Common.Wrappers;
using Application.DTOs.Address;
using Application.Interfaces.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Address.Queries
{
    public class GetAddressByIdQuery : IRequest<Response<AddressResponseDto>>
    {
        public int Id { get; set; }

        public class GetAddressByIdQueryHandler : IRequestHandler<GetAddressByIdQuery,
            Response<AddressResponseDto>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public GetAddressByIdQueryHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<AddressResponseDto>> Handle(GetAddressByIdQuery request, CancellationToken cancellationToken)
            {
                var entity = await _unitOfWork.Address.GetAsync<AddressResponseDto>(a => a.Id == request.Id);

                return new Response<AddressResponseDto>(entity);
            }
        }
    }
}