﻿using Application.Common.Parameters;
using Application.Common.Wrappers;
using Application.DTOs.Address;
using Application.DTOs.Customer;
using Application.Interfaces.Repositories;
using Mapster;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Address.Queries
{

    public class GetAllAddressQuery : IRequest<Response<IEnumerable<AddressResponseDto>>>
    {
        public class GetAllAddressQueryHandler : IRequestHandler<GetAllAddressQuery,
            Response<IEnumerable<AddressResponseDto>>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public GetAllAddressQueryHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<IEnumerable<AddressResponseDto>>> Handle(GetAllAddressQuery request, CancellationToken cancellationToken)
            {
                var entity = await _unitOfWork.Address.GetAllAsync<AddressResponseDto>();

                return new Response<IEnumerable<AddressResponseDto>>(entity);
            }
        }
    }
}