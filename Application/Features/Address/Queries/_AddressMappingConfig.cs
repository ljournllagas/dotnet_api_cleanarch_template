﻿using Application.DTOs.Address;
using Application.DTOs.Customer;
using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Features.Address.Queries
{
    public class _AddressMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<Domain.Entities.Address, AddressResponseDto>();

            config.ForType<Domain.Entities.Address, AddressFullDetailsResponseDto>()
                .Map(d => d.CustomerFirstName, src => src.Customer.FirstName)
                .Map(d => d.CustomerLastName, src => src.Customer.LastName)
                .Map(d => d.EmailAddress, src => src.Customer.EmailAddress)
                .Map(d => d.DateOfBirth, src => src.Customer.DateOfBirth);
        }
    }
}
