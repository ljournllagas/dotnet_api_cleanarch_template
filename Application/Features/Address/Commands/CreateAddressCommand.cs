﻿using Application.Common.Wrappers;
using Application.Constants;
using Application.DTOs.Address;
using Application.Interfaces.Repositories;
using Mapster;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Address.Commands
{

    public class CreateAddressCommand : IRequest<Response<int>>, IRegister
    {
        public AddressPostRequestDto dto { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddressPostRequestDto, Domain.Entities.Address>();
        }

        public class CreateAddressCommandHandler : IRequestHandler<CreateAddressCommand, Response<int>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public CreateAddressCommandHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<int>> Handle(CreateAddressCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var entity = request.dto.Adapt<Domain.Entities.Address>();

                    await _unitOfWork.Address.AddAsync(entity);

                    await _unitOfWork.CommitAsync();

                    return new Response<int>(entity.Id,
                                             new CrudResponse(CrudOperation.Create));
                }
                catch (Exception)
                {
                    await _unitOfWork.RollbackAsync();
                    throw;
                }
            }
        }

    }
}
