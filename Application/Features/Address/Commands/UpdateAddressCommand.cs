﻿using Application.Common.Exceptions;
using Application.Common.Wrappers;
using Application.Constants;
using Application.DTOs.Address;
using Application.Interfaces.Repositories;
using Mapster;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Address.Commands
{
    public class UpdateAddressCommand : IRequest<Response<int>>, IRegister
    {
        public AddressPutRequestDto dto { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddressPutRequestDto, Domain.Entities.Address>();
        }

        public class UpdateAddressCommandHandler : IRequestHandler<UpdateAddressCommand, Response<int>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public UpdateAddressCommandHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<int>> Handle(UpdateAddressCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var entity = await _unitOfWork.Address.GetByIdAsync(request.dto.Id);

                    if (entity == null)
                    {
                        throw new NotFoundException(nameof(Domain.Entities.Address), nameof(request.dto.Id), request.dto.Id);
                    }

                    request.dto.Adapt(entity);

                    await _unitOfWork.CommitAsync();

                    return new Response<int>(entity.Id,
                                             new CrudResponse(CrudOperation.Update));
                }
                catch (Exception)
                {
                    await _unitOfWork.RollbackAsync();
                    throw;
                }
            }
        }

    }
}
