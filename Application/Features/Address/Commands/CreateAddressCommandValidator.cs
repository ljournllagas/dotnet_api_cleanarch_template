﻿using FluentValidation;

namespace Application.Features.Address.Commands
{
    public class CreateAddressCommandValidator : AbstractValidator<CreateAddressCommand>
    {
        public CreateAddressCommandValidator()
        {
            RuleFor(v => v.dto.FullAddress)
                .MaximumLength(200)
                .NotEmpty();

            RuleFor(v => v.dto.ZipCode)
                .NotEmpty();
        }
    }
}
