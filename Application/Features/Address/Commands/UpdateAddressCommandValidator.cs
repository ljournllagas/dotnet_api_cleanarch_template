﻿using FluentValidation;

namespace Application.Features.Address.Commands
{
    public class UpdateAddressCommandValidator : AbstractValidator<UpdateAddressCommand>
    {
        public UpdateAddressCommandValidator()
        {
            RuleFor(v => v.dto.FullAddress)
                .MaximumLength(200)
                .NotEmpty();

            RuleFor(v => v.dto.ZipCode)
                .NotEmpty();
        }
    }
}
