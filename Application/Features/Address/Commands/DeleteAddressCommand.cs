﻿using Application.Common.Exceptions;
using Application.Common.Wrappers;
using Application.Constants;
using Application.Interfaces.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Address.Commands
{
    public class DeleteAddressCommand : IRequest<Response<int>>
    {
        public int AddressId { get; set; }

        public class DeleteAddressCommandHandler : IRequestHandler<DeleteAddressCommand, Response<int>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public DeleteAddressCommandHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<int>> Handle(DeleteAddressCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var entity = await _unitOfWork.Address.GetByIdAsync(request.AddressId);

                    if (entity == null)
                    {
                        throw new NotFoundException(nameof(Domain.Entities.Address), nameof(request.AddressId), request.AddressId);
                    }

                    _unitOfWork.Address.Delete(entity);

                    await _unitOfWork.CommitAsync();

                    return new Response<int>(entity.Id,
                        new CrudResponse(CrudOperation.Delete));
                }
                catch (System.Exception)
                {
                    await _unitOfWork.RollbackAsync();
                    throw;
                }
            }
        }
    }
}
