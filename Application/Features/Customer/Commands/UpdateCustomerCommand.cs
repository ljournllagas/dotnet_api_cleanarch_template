﻿using Application.Common.Exceptions;
using Application.Common.Wrappers;
using Application.Constants;
using Application.DTOs.Address;
using Application.DTOs.Customer;
using Application.Interfaces.Repositories;
using Mapster;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Customer.Commands
{
    public class UpdateCustomerCommand : IRequest<Response<Guid>>, IRegister
    {
        public CustomerPutRequestDto dto { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.ForType<CustomerPutRequestDto, Domain.Entities.Customer>();
        }

        public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, Response<Guid>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public UpdateCustomerCommandHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<Guid>> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var entity = await _unitOfWork.Customer.GetByIdAsync(request.dto.Id);

                    if (entity == null)
                    {
                        throw new NotFoundException(nameof(Domain.Entities.Customer), nameof(request.dto.Id), request.dto.Id);
                    }

                    request.dto.Adapt(entity);

                    await _unitOfWork.CommitAsync();

                    return new Response<Guid>(entity.Id,
                                             new CrudResponse(CrudOperation.Update));
                }
                catch (Exception)
                {
                    await _unitOfWork.RollbackAsync();
                    throw;
                }
            }
        }

    }
}
