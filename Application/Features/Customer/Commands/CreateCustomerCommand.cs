﻿using Application.Common.Wrappers;
using Application.Constants;
using Application.DTOs.Customer;
using Application.Interfaces.Repositories;
using Mapster;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Customer.Commands
{

    public class CreateCustomerCommand : IRequest<Response<Guid>>, IRegister
    {
        public CustomerPostRequestDto dto { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.ForType<CustomerPostRequestDto, Domain.Entities.Customer>();
        }

        public class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, Response<Guid>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public CreateCustomerCommandHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<Guid>> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var entity = request.dto.Adapt<Domain.Entities.Customer>();

                    await _unitOfWork.Customer.AddAsync(entity);

                    await _unitOfWork.CommitAsync();

                    return new Response<Guid>(entity.Id,
                        new CrudResponse(CrudOperation.Create));
                }
                catch (Exception)
                {
                    await _unitOfWork.RollbackAsync();
                    throw;
                }
            }
        }

    }
}
