﻿using Application.Common.Extensions;
using FluentValidation;

namespace Application.Features.Customer.Commands
{
    public class UpdateCustomerCommandValidator : AbstractValidator<UpdateCustomerCommand>
    {
        public UpdateCustomerCommandValidator()
        {
            RuleFor(v => v.dto.FirstName)
               .MaximumLength(50)
               .NotEmpty();

            RuleFor(v => v.dto.LastName)
                .MaximumLength(50)
                .NotEmpty();

            RuleFor(v => v.dto.EmailAddress)
                .MustHaveMaximumLength(50);

            RuleFor(v => v.dto.DateOfBirth)
                .NotEmpty();
        }
    }
}
