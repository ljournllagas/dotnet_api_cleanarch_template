﻿using Application.Common.Exceptions;
using Application.Common.Wrappers;
using Application.Constants;
using Application.Interfaces.Repositories;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Customer.Commands
{
    public class DeleteCustomerCommand : IRequest<Response<Guid>>
    {
        public Guid CustomerId { get; set; }

        public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, Response<Guid>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public DeleteCustomerCommandHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<Guid>> Handle(DeleteCustomerCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var entity = await _unitOfWork.Customer.GetByIdAsync(request.CustomerId);

                    if (entity == null)
                    {
                        throw new NotFoundException(nameof(Domain.Entities.Customer), nameof(request.CustomerId), request.CustomerId);
                    }

                    _unitOfWork.Customer.Delete(entity);

                    await _unitOfWork.CommitAsync();

                    return new Response<Guid>(entity.Id,
                        new CrudResponse(CrudOperation.Delete));
                }
                catch (System.Exception)
                {
                    await _unitOfWork.RollbackAsync();
                    throw;
                }
            }
        }
    }
}
