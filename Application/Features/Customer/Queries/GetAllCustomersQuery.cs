﻿using Application.Common.Parameters;
using Application.Common.Wrappers;
using Application.DTOs.Customer;
using Application.Interfaces.Repositories;
using Mapster;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Maintenance.ParameterCitizenship.Queries.GetAllParameterCitizenship
{
    public class GetAllCustomersQuery : IRequest<Response<IEnumerable<CustomerResponseDto>>>
    {
        public class GetAllCustomersQueryHandler : IRequestHandler<GetAllCustomersQuery,
            Response<IEnumerable<CustomerResponseDto>>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public GetAllCustomersQueryHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<IEnumerable<CustomerResponseDto>>> Handle(GetAllCustomersQuery request, CancellationToken cancellationToken)
            {
                var entity = await _unitOfWork.Customer.GetAllAsync<CustomerResponseDto>();

                return new Response<IEnumerable<CustomerResponseDto>>(entity);
            }
        }

    }
}