﻿using Application.DTOs.Customer;
using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Features.Customer.Queries
{
    public class _AddressMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<Domain.Entities.Customer, CustomerResponseDto>
             ();

        }
    }
}
