﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Persistence.FluentEntityConfigs
{
    public class CustomerOrdersConfig : IEntityTypeConfiguration<CustomerOrders>
    {
        public void Configure(EntityTypeBuilder<CustomerOrders> builder)
        {
            builder.HasKey(t => t.OrderId);

            builder
               .HasOne(b => b.Customer)
               .WithMany(b => b.CustomerOrders)
               .HasForeignKey(b => b.CustomerId);

            builder
               .HasOne(o => o.Product)
               .WithMany(m => m.CustomerOrders)
               .HasForeignKey(f => f.ProductId);
        }
    }

}
