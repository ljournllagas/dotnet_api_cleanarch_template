﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Persistence.FluentEntityConfigs
{
    public class ProductConfig : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(b => b.Id);

            builder.Property(a => a.Name).HasMaxLength(50).IsRequired();

            builder.Property(a => a.Price).IsRequired();

            builder.Property(a => a.ReleaseDate).IsRequired();
        }
    }

}
