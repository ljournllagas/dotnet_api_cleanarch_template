﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Persistence.FluentEntityConfigs
{
    public class CustomerConfig : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(b => b.Id);

            builder.Property(a => a.FirstName).HasMaxLength(50).IsRequired();

            builder.Property(a => a.LastName).HasMaxLength(50).IsRequired();

            builder.Property(a => a.EmailAddress).HasMaxLength(50).IsRequired(false);

            builder.Property(a => a.DateOfBirth).IsRequired();

        }
    }

}
