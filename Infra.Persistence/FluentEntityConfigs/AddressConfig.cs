﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Persistence.FluentEntityConfigs
{
    public class AddressConfig : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.HasKey(b => b.Id);

            builder.Property(a => a.FullAddress).HasMaxLength(200).IsRequired();

            builder.Property(a => a.ZipCode).IsRequired();

            builder.HasOne(b => b.Customer)
                    .WithMany(b => b.Addresses)
                    .HasForeignKey(b => b.CustomerId);
        }
    }

}
