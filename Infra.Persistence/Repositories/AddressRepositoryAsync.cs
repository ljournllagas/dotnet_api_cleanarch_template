﻿using Application.DTOs.Address;
using Application.Interfaces.Repositories;
using Domain.Entities;
using Infra.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Infra.Persistence.Repositories
{
    public class AddressRepositoryAsync : GenericRepositoryAsync<Address>, IAddressRepositoryAsync
    {
        private readonly DbSet<Address> _db;

        public AddressRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _db = dbContext.Set<Address>();
        }
    }
}
