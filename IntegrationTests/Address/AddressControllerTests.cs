﻿using Application.Common.Wrappers;
using Application.DTOs.Address;
using FluentAssertions;
using HLFS.IntegrationTests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests.Address
{
    public class AddressControllerTests : IntegrationTest
    {
        const string _baseApiEndpoint = "/api/address";

        [Fact]
        public async Task GetAddressById_ReturnsPost_WhenPostExistsInTheDatabase()
        {
            //Arrange
            await AuthenticateAsync();

            string address = "Antipolo City";
            int zipCode = 1870;

            //Act
            var callApi = await TestClient.PostAsJsonAsync($"{_baseApiEndpoint}/Create", new AddressPostRequestDto()
            {
                CustomerId = new Guid(),
                FullAddress = address,
                ZipCode = zipCode
            });

            var callApiResponse = await callApi.Content.ReadFromJsonAsync<Response<int>>();

            //Assert
            var getThePostedDataByItsId = await TestClient.GetAsync($"{_baseApiEndpoint}/{callApiResponse.Data}");

            var response = await getThePostedDataByItsId.Content.ReadFromJsonAsync<Response<AddressResponseDto>>();

            response.Succeeded.Should().BeTrue();

            response.Data.Should().NotBeNull();

            response.Data.Id.Should().Be(callApiResponse.Data);

            response.Data.FullAddress.Should().BeEquivalentTo(address);

            response.Data.ZipCode.Should().Be(zipCode);
        }
    }
}
