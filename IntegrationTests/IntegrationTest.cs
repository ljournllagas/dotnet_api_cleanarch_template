﻿using Application.Common.Wrappers;
using Application.DTOs.Authentication.Request;
using Application.DTOs.Authentication.Response;
using Infra.Persistence.Contexts;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using WebApi;

namespace HLFS.IntegrationTests
{
    public class IntegrationTest
    {
        protected readonly HttpClient TestClient;
        protected readonly ApplicationDbContext _dbContext;
        protected IntegrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>()
                             .WithWebHostBuilder(builder =>
                             {
                                 builder.ConfigureServices(services =>
                                 {
                                     var dbContextService = services.SingleOrDefault(d => d.ServiceType ==
                                                            typeof(DbContextOptions<ApplicationDbContext>)
                                                            );

                                     if (dbContextService != null)
                                     {
                                         services.Remove(dbContextService);
                                     }

                                     services.AddDbContext<ApplicationDbContext>(options =>
                                     {
                                         options.UseInMemoryDatabase("TestDb");
                                     });

                                 });
                             });

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                            .UseInMemoryDatabase("TestDb").Options;

            var dbContext = new ApplicationDbContext(options);

            dbContext.SaveChanges();

            TestClient = appFactory.CreateClient();
        }

        protected async Task AuthenticateAsync()
        {
            TestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", await GetJwtAsync());
        }

        private async Task<string> GetJwtAsync()
        {
            //login to get the jwt
            var responseLogin = await TestClient.PostAsJsonAsync("/api/auth/login", new LoginRequestDto()
            {
                Username = "test-user",
                Password = "p@55w0rd123"
            });

            var loginResponse = await responseLogin.Content.ReadFromJsonAsync<Response<UserToken>>();

            return loginResponse.Data.Token;
        }

    }
}
