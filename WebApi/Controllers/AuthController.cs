﻿using Application.DTOs.Authentication.Request;
using Application.Features.Authentication;
using Application.Interfaces.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers.Authentication
{
    public class AuthController : BaseApiController
    {
        /// <summary>
        /// Login by username/password
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>JWT authentication token and its expiration date/time</returns>
        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> AuthenticateAsync(LoginRequestDto dto)
        {
            return Ok(await Mediator.Send(new LoginCommand() { dto = dto }));
        }

    }
}