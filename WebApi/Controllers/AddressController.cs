﻿using Application.DTOs.Address;
using Application.Features.Address.Commands;
using Application.Features.Address.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Authorize]
    public class AddressController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Create(AddressPostRequestDto dto)
        {
            return Ok(await Mediator.Send(new CreateAddressCommand() { dto = dto }));
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(AddressPutRequestDto dto)
        {
            return Ok(await Mediator.Send(new UpdateAddressCommand() { dto = dto }));
        }

        [HttpDelete("Delete/{addressId}")]
        public async Task<IActionResult> Delete(int addressId)
        {
            return Ok(await Mediator.Send(new DeleteAddressCommand() { AddressId = addressId }));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetAddressByIdQuery() { Id = id }));
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllAddressQuery()));
        }

        [HttpGet("GetAllFullAddressDetails")]
        public async Task<IActionResult> GetAllFullAddressDetails()
        {
            return Ok(await Mediator.Send(new GetAddressWithFullDetailsQuery()));
        }
    }
}
