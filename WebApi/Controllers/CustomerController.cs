﻿using Application.DTOs.Customer;
using Application.Features.Address.Queries;
using Application.Features.Customer.Commands;
using Application.Features.Maintenance.ParameterCitizenship.Queries.GetAllParameterCitizenship;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Authorize]
    public class CustomerController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Create(CustomerPostRequestDto dto)
        {
            return Ok(await Mediator.Send(new CreateCustomerCommand() { dto = dto }));
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(CustomerPutRequestDto dto)
        {
            return Ok(await Mediator.Send(new UpdateCustomerCommand() { dto = dto }));
        }

        [HttpDelete("Delete/{CustomerId}")]
        public async Task<IActionResult> Delete(Guid CustomerId)
        {
            return Ok(await Mediator.Send(new DeleteCustomerCommand() { CustomerId = CustomerId }));
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllCustomersQuery()));
        }
    }
}
