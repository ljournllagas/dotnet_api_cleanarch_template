﻿using Application.Interfaces.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    public class MetaController : BaseApiController
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public MetaController(IWebHostEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        public class AppSettingsKeyValuePair
        {
            public string Key { get; set; }

            public string Value { get; set; }
        }

        public class InfoResponse
        {
            public string LastUpdate { get; set; }
            public string ApiVersion { get; set; }
            public string Environment { get; set; }
            public string DbConnection { get; set; }
            public List<AppSettingsKeyValuePair> appSettings { get; set; }
        }


        [HttpGet("/downloadrequestlog")]
        public async Task<FileStreamResult> GetRequestLog()
        {
            var logDirectory = _configuration.GetSection("Serilog:Properties:TextFileLogDir").Value;

            var customPath = $"{logDirectory}Custom";

            var customDirectory = new DirectoryInfo(customPath);

            var latestRequestLogFile = (from f in customDirectory.GetFiles()
                                        orderby f.LastWriteTime descending
                                        select f).First(a => a.FullName.Contains("RequestLogs"));

            if (latestRequestLogFile != null)
            {
                var fullPath = Path.Combine(customPath, latestRequestLogFile.FullName);

                System.IO.File.Copy(fullPath, Path.Combine(customPath, "latestrequestlog.txt"), true);

                byte[] byteAttachment = await System.IO.File.ReadAllBytesAsync(Path.Combine(customPath, "latestrequestlog.txt"));

                var stream = new MemoryStream(byteAttachment);

                return new FileStreamResult(stream, new MediaTypeHeaderValue("application/octet-stream"))
                {
                    FileDownloadName = latestRequestLogFile.FullName
                };
            }

            return new FileStreamResult(new MemoryStream(), new MediaTypeHeaderValue("application/octet-stream"))
            {
                FileDownloadName = "blank.txt"
            };
        }

        [HttpGet("/downloadlog")]
        #pragma warning disable S4144 // Methods should not have identical implementations
        public async Task<FileStreamResult> GetLatestLog()
        #pragma warning restore S4144 // Methods should not have identical implementations
        {
            var logDirectory = _configuration.GetSection("Serilog:Properties:TextFileLogDir").Value;

            var directory = new DirectoryInfo(logDirectory);

            var latestLogFile = (from f in directory.GetFiles()
                                 orderby f.LastWriteTime descending
                                 select f).First();

            var fullPath = Path.Combine(logDirectory, latestLogFile.FullName);

            System.IO.File.Copy(fullPath, Path.Combine(logDirectory, "latestlog.txt"), true);

            byte[] byteAttachment = await System.IO.File.ReadAllBytesAsync(Path.Combine(logDirectory, "latestlog.txt"));

            var stream = new MemoryStream(byteAttachment);

            return new FileStreamResult(stream, new MediaTypeHeaderValue("application/octet-stream"))
            {
                FileDownloadName = latestLogFile.FullName
            };
        }

        [HttpGet("/version")]
        public IActionResult Version()
        {
            var assembly = typeof(Startup).Assembly;

            var lastUpdate = System.IO.File.GetLastWriteTime(assembly.Location);
            var version = FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;

            return Ok(
                new
                {
                    ApiVersion = version,
                    LastUpdate = lastUpdate.ToString("MM/dd/yyyy hh:mm tt")
                }
           );
        }

        /// <summary>
        /// Get the API program info (last updated date/time and current version)
        /// </summary>
        /// <returns></returns>
        [HttpGet("/info")]
        public IActionResult Info()
        {

            var assembly = typeof(Startup).Assembly;

            var lastUpdate = System.IO.File.GetLastWriteTime(assembly.Location);
            var version = FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;
            var environment = _hostingEnvironment.EnvironmentName;
            var dbConnection = _configuration.GetConnectionString("DefaultConnection");

            var appSettings = new List<AppSettingsKeyValuePair>();

            var configs = _configuration.AsEnumerable();

            foreach (var item in configs)
            {
                appSettings.Add(new AppSettingsKeyValuePair()
                {
                    Key = item.Key,
                    Value = item.Value
                });
            }

            return Ok(
                new InfoResponse()
                {
                    ApiVersion = version,
                    appSettings = appSettings,
                    DbConnection = dbConnection,
                    Environment = environment,
                    LastUpdate = lastUpdate.ToString("MM/dd/yyyy hh:mm tt")
                }
            );
        }
    }
}
