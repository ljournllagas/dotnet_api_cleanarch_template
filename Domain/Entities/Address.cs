﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Address
    {
        public int Id { get; set; }

        public string FullAddress { get; set; }

        public int ZipCode { get; set; }

        public Guid CustomerId { get; set; }

        public Customer Customer { get; set; }
    }
}
