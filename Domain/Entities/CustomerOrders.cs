﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class CustomerOrders
    {
        public int OrderId { get; set; }

        public Guid CustomerId { get; set; }

        public int ProductId { get; set; }

        public Customer Customer { get; set; }

        public Product Product { get; set; }
    }
}
