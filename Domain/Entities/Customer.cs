﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Customer
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age => DateTimeOffset.Now.Year - DateOfBirth.Year;

        public string EmailAddress { get; set; }

        public DateTimeOffset DateOfBirth { get; set; }

        public bool InLegalAge => Age >= 18;

        public ICollection<Address> Addresses { get; set; }

        public ICollection<CustomerOrders> CustomerOrders { get; set; }

    }
}
